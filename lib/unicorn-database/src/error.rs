#[derive(Debug)]
pub enum UnicornDatabaseError {
    MongoDBError(mongodb::error::Error),
}

impl From<mongodb::error::Error> for UnicornDatabaseError {
    fn from(err: mongodb::error::Error) -> Self {
        UnicornDatabaseError::MongoDBError(err)
    }
}
