use bson::*;
use log::error;
use mongodb::options::UpdateOptions;
use serde::{Deserialize, Serialize};

use unicorn_cache::handler::CacheHandler;

use crate::DatabaseHandler;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct GuildSettings {
    pub server_id: i64,
    #[serde(default = "GuildSettings::default_option_str")]
    pub prefix: Option<String>,
    #[serde(default = "GuildSettings::default_bool")]
    pub unflip: bool,
    #[serde(default = "GuildSettings::default_vec_str")]
    pub blocked_words: Vec<String>,
    #[serde(default = "GuildSettings::default_bool")]
    pub ascii_only_names: bool,
    #[serde(default = "GuildSettings::default_option_str")]
    pub ascii_temp_name: Option<String>,
}

// Default Methods
impl GuildSettings {
    fn default_option_str() -> Option<String> {
        None
    }

    fn default_bool() -> bool {
        false
    }

    fn default_vec_str() -> Vec<String> {
        Vec::default()
    }
}

// Useable Methods
impl GuildSettings {
    pub fn new(gid: u64) -> Self {
        Self{ server_id: gid as i64, ..Self::default()}
    }

    fn to_bson(&self) -> Option<Bson> {
        match bson::to_bson(self) {
            Ok(bs) => Some(bs),
            Err(why) => {
                error!("Failed serializing settings BSON: {}", why);
                None
            }
        }
    }

    async fn get_from_cache(cache: &CacheHandler, gid: u64) -> Option<Self> {
        match cache.get(format!("settings:{}", gid)).await {
            Some(settings_body) => {
                let res = serde_json::from_str::<GuildSettings>(&settings_body);
                match res {
                    Ok(settings) => Some(settings),
                    Err(why) => {
                        error!("Cached settings deserialization failed: {}", why);
                        None
                    }
                }
            }
            None => None,
        }
    }

    async fn save_to_cache(cache: &CacheHandler, settings: Self) {
        match serde_json::to_string(&settings) {
            Ok(settings_body) => {
                cache.set(format!("settings:{}", &settings.server_id), settings_body).await;
            }
            Err(why) => {
                error!("Cached settings serialization failed: {}", why);
            }
        }
    }

    async fn get_from_database(db: &DatabaseHandler, gid: u64) -> Option<Self> {
        if let Some(cli) = db.get_client() {
            {
                let lookup = doc! {"server_id": gid as i64};
                match cli
                    .database(&db.cfg.database.name)
                    .collection::<bson::Document>("server_settings")
                    .find_one(lookup, None)
                    .await
                {
                    Ok(sopt) => match sopt {
                        Some(sdoc) => {
                            let sdoc = bson::Bson::Document(sdoc);
                            let sres = bson::from_bson::<Self>(sdoc);
                            match sres {
                                Ok(sset) => Some(sset),
                                Err(_) => None,
                            }
                        }
                        None => None,
                    },
                    Err(why) => {
                        error!("Failed retrieving settings: {}", why);
                        None
                    }
                }
            }
        } else {
            None
        }
    }

    async fn save_to_database(db: &DatabaseHandler, settings: Self) {
        if let Some(bdat) = settings.to_bson() {
            let mut data = doc! {};
            data.insert("$set", bdat);
            let lookup = doc! {"server_id": settings.server_id};
            let opts = UpdateOptions::builder().upsert(true).build();
            if let Some(cli) = db.get_client() {
                if let Err(why) = cli
                    .database(&db.cfg.database.name)
                    .collection::<bson::Document>("server_settings")
                    .update_one(lookup, data, opts)
                    .await
                {
                    error!("Failed updating settings for guild {}: {}", settings.server_id, why);
                }
            }
        }
    }

    pub async fn get(db: &DatabaseHandler, gid: u64) -> Self {
        match Self::get_from_cache(&db.cache, gid).await {
            Some(settings) => settings,
            None => match Self::get_from_database(db, gid).await {
                Some(settings) => {
                    Self::save_to_cache(&db.cache, settings.clone()).await;
                    settings
                }
                None => Self::new(gid),
            },
        }
    }

    pub async fn save(&self, db: &DatabaseHandler) {
        Self::save_to_cache(&db.cache, self.clone()).await;
        Self::save_to_database(db, self.clone()).await;
    }
}
