pub enum ResourceOrigin {
    UserId(serenity::model::id::UserId),
    ChannelId(serenity::model::id::ChannelId),
    GuildId(serenity::model::id::GuildId),
}

impl From<serenity::model::id::UserId> for ResourceOrigin {
    fn from(id: serenity::model::id::UserId) -> Self {
        ResourceOrigin::UserId(id)
    }
}

impl From<serenity::model::id::ChannelId> for ResourceOrigin {
    fn from(id: serenity::model::id::ChannelId) -> Self {
        ResourceOrigin::ChannelId(id)
    }
}

impl From<serenity::model::id::GuildId> for ResourceOrigin {
    fn from(id: serenity::model::id::GuildId) -> Self {
        ResourceOrigin::GuildId(id)
    }
}
