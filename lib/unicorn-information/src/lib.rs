#[derive(Debug, Clone)]
pub struct ModuleInformation {
    pub name: String,
    pub commands: Vec<CommandInformation>,
}

#[derive(Debug, Clone)]
pub struct CommandInformation {
    pub name: String,
    pub category: String,
    pub nsfw: bool,
    pub owner: bool,
    pub dm: bool,
    pub aliases: Vec<String>,
    pub example: String,
    pub description: String,
}

#[derive(Debug, Clone)]
pub struct EventInformation {
    pub name: String,
    pub category: String,
    pub trigger: String,
}
