use std::any::Any;

use fern::Dispatch;

use unicorn_config::builder::ConfigurationBuilder;
use unicorn_database::DatabaseHandler;

use crate::UnicornCommand;
use crate::UnicornEvent;
use crate::UnicornReaction;

#[serenity::async_trait]
pub trait UnicornModule: Any + Send + Sync {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>>;
    fn reactions(&self) -> Vec<Box<dyn UnicornReaction>> {
        Vec::new()
    }
    fn events(&self) -> Vec<Box<dyn UnicornEvent>>;
    fn name(&self) -> &str;
    async fn on_load(&self, _db: &DatabaseHandler, _cfg: &mut ConfigurationBuilder) {}
    fn init_logger(&self, level: log::LevelFilter) {
        if let Ok(logfile) = fern::log_file(format!("logs/{}_unc_{}.log", self.name(), chrono::Local::now().format("%Y-%m-%d"))) {
            let _ = Dispatch::new()
                .format(|out, message, record| {
                    out.finish(format_args!(
                        "{}[{}][{}] {}",
                        chrono::Local::now().format("[%Y-%m-%d %H:%M:%S]"),
                        record.target(),
                        record.level(),
                        message
                    ))
                })
                .level(level)
                .chain(std::io::stdout())
                .chain(logfile)
                .apply();
        }
    }
}
