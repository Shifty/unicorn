use log::error;
use serenity::prelude::ClientError;
use serenity::{Client, Error};

use unicorn_config::config::Configuration;

use crate::error::UnicornClientError;
use crate::handler::UnicornHandler;
use serenity::client::bridge::gateway::GatewayIntents;

pub struct UnicornClient {
    cfg: Configuration,
    client: Client,
}

impl UnicornClient {
    pub async fn new(mut cfg: Configuration) -> Result<Self, UnicornClientError> {
        let handler = UnicornHandler::new(&mut cfg).await?;
        let client = Client::builder(&cfg.discord.token)
            .intents(GatewayIntents::all())
            .event_handler(handler)
            .await?;
        cfg.describe();
        Ok(Self { cfg, client })
    }

    pub async fn run(&mut self) -> Result<(), UnicornClientError> {
        if !&self.cfg.discord.token.is_empty() {
            self.client.start_autosharded().await?;
            Ok(())
        } else {
            error!("The token is empty, shutting down.");
            Err(UnicornClientError::SerenityError(Error::Client(
                ClientError::InvalidToken,
            )))
        }
    }
}
