pub enum UnicornCacheError {
    RedisError(redis::RedisError),
}

impl From<redis::RedisError> for UnicornCacheError {
    fn from(err: redis::RedisError) -> Self {
        Self::RedisError(err)
    }
}
