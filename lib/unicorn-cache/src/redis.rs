use log::{debug, error, info};
use redis::{Client, RedisError};

use unicorn_config::cache::CacheConfig;

use crate::error::UnicornCacheError;

#[derive(Debug, Clone)]
pub struct RedisCacher {
    ttl: Option<u64>,
    client: Client,
}

impl RedisCacher {
    pub async fn get(&self, key: String) -> Option<String> {
        match self.get_conn().await {
            Ok(mut conn) => {
                let res: Result<Option<String>, RedisError> = redis::cmd("GET").arg(&key).query_async(&mut conn).await;
                match res {
                    Ok(val) => {
                        debug!(
                            "Redis GET for \"{}\" returned \"{}\".",
                            key,
                            val.clone().unwrap_or_else(|| "<None>".to_owned())
                        );
                        val
                    }
                    Err(why) => {
                        error!("Failed getting \"{}\" cache key!: {}", key, why);
                        None
                    }
                }
            }
            Err(_why) => {
                error!("Failed getting redis connection from pool!");
                None
            }
        }
    }

    pub async fn set(&self, key: String, value: String) {
        match self.get_conn().await {
            Ok(mut conn) => {
                let res: Result<Option<bool>, RedisError> = redis::cmd("SET").arg(&key).arg(&value).query_async(&mut conn).await;
                match res {
                    Ok(rv) => {
                        let success = rv.unwrap_or(false);
                        if success {
                            if let Some(ttl) = self.ttl {
                                let ttl_res: Result<Option<bool>, RedisError> = redis::cmd("EXPIRE").arg(&key).arg(ttl).query_async(&mut conn).await;
                                match ttl_res {
                                    Ok(ttl_rv) => debug!(
                                        "Expiration setting key {} on a ttl of {}s resulted in {}",
                                        &key,
                                        ttl,
                                        if ttl_rv.unwrap_or(false) { "success" } else { "failure" }
                                    ),
                                    Err(why) => error!("Failed setting a {}s TTL for {}: {}", ttl, &key, why),
                                }
                            }
                            debug!("Set key \"{}\" with value \"{}\".", key, value);
                        } else {
                            error!("Setting \"{}\" cache key resulted in a failed response!", key);
                        }
                    }
                    Err(why) => {
                        error!("Failed setting \"{}\" cache key!: {}", key, why);
                    }
                }
            }
            Err(_why) => {
                error!("Failed getting redis connection from pool!");
            }
        }
    }

    pub async fn del(&self, key: String) {
        match self.get_conn().await {
            Ok(mut conn) => {
                let res: Result<(), RedisError> = redis::cmd("DEL").arg(&key).query_async(&mut conn).await;
                match res {
                    Ok(_) => {
                        debug!("Deleted key \"{}\".", key);
                    }
                    Err(why) => {
                        error!("Failed deleting {} cache key!: {}", key, why);
                    }
                };
            }
            Err(_why) => {
                error!("Failed getting redis connection from pool!");
            }
        }
    }

    pub async fn wipe(&self) {
        match self.get_conn().await {
            Ok(mut conn) => {
                let res: Result<(), RedisError> = redis::cmd("FLUSHDB").query_async(&mut conn).await;
                match res {
                    Ok(_) => {
                        debug!("Flushed redis database.");
                    }
                    Err(why) => {
                        error!("Failed to flush the database cache key!: {}", why);
                    }
                };
            }
            Err(_why) => {
                error!("Failed getting redis connection from pool!");
            }
        }
    }

    async fn get_conn(&self) -> Result<redis::aio::Connection, UnicornCacheError> {
        Ok(self.client.get_async_connection().await?)
    }

    pub fn new(cfg: &CacheConfig) -> Result<Self, UnicornCacheError> {
        let address = format!("redis://{}:{}/{}", &cfg.host, &cfg.port, &cfg.db);
        let client = redis::Client::open(address)?;
        info!("Redis cache connected!");
        Ok(Self { ttl: cfg.ttl, client })
    }
}
