use crate::common::ConfigLoader;

dyn_clone::clone_trait_object!(ConfigLoader);

#[derive(Debug, Clone)]
pub struct CustomConfig {
    cfg: Box<dyn ConfigLoader>
}


impl CustomConfig {
    pub fn new(cfg: Box<dyn ConfigLoader>) -> Self {
        Self {
            cfg
        }
    }

    pub fn open<T: 'static>(&self) -> Option<&T> {
        self.cfg.as_any().downcast_ref::<T>()
    }

    pub fn describe(&self) {
        self.cfg.describe();
    }
}

