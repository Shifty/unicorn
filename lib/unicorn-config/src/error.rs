use std::fmt::{Display, Formatter, Result};

pub enum UnicornConfigError {
    IOError(std::io::Error),
    SerdeYAMLError(serde_yaml::Error),
    SerdeJSONError(serde_json::Error),
}

impl From<std::io::Error> for UnicornConfigError {
    fn from(err: std::io::Error) -> Self {
        UnicornConfigError::IOError(err)
    }
}

impl From<serde_yaml::Error> for UnicornConfigError {
    fn from(err: serde_yaml::Error) -> Self {
        UnicornConfigError::SerdeYAMLError(err)
    }
}

impl From<serde_json::Error> for UnicornConfigError {
    fn from(err: serde_json::Error) -> Self {
        UnicornConfigError::SerdeJSONError(err)
    }
}

impl Display for UnicornConfigError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match *self {
            UnicornConfigError::IOError(ref err) => write!(f, "{}", err),
            UnicornConfigError::SerdeYAMLError(ref err) => write!(f, "{})", err),
            UnicornConfigError::SerdeJSONError(ref err) => write!(f, "{})", err),
        }
    }
}
