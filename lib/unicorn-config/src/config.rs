use std::collections::HashMap;

use crate::builder::ConfigurationBuilder;
use crate::cache::CacheConfig;
use crate::common::ConfigLoader;
use crate::custom::CustomConfig;
use crate::database::DatabaseConfig;
use crate::discord::DiscordConfig;
use crate::preferences::PreferencesConfig;

#[derive(Debug, Clone)]
pub struct Configuration {
    pub discord: DiscordConfig,
    pub database: DatabaseConfig,
    pub preferences: PreferencesConfig,
    pub cache: CacheConfig,
    custom: HashMap<String, CustomConfig>,
}

impl Default for Configuration {
    fn default() -> Self {
        Self {
            discord: DiscordConfig::new().unwrap_or_else(|e| {
                println!("{}", e);
                DiscordConfig::default()
            }),
            database: DatabaseConfig::new().unwrap_or_else(|e| {
                println!("{}", e);
                DatabaseConfig::default()
            }),
            preferences: PreferencesConfig::new().unwrap_or_else(|e| {
                println!("{}", e);
                PreferencesConfig::default()
            }),
            cache: CacheConfig::new().unwrap_or_else(|e| {
                println!("{}", e);
                CacheConfig::default()
            }),
            custom: HashMap::new(),
        }
    }
}

impl Configuration {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn add_custom(&mut self, builder: ConfigurationBuilder) {
        self.custom = builder.collect_custom();
    }

    pub fn get_custom<T: 'static>(&self) -> Option<&T> {
        let name = std::any::type_name::<T>();
        if let Some(cfg) = self.custom.get(name) {
            cfg.open::<T>()
        } else {
            None
        }
    }

    pub fn describe(&self) {
        self.discord.describe();
        self.database.describe();
        self.preferences.describe();
        self.cache.describe();
        for config in self.custom.values() {
            config.describe();
        }
    }
}
