pub struct UnicornString;

impl UnicornString {
    pub fn connector(text: &str) -> String {
        if let Some(chr) = text.chars().collect::<Vec<char>>().get(0) {
            if vec!['a', 'e', 'i', 'o', 'u'].contains(chr) {
                "an"
            } else {
                "a"
            }
        } else {
            "a"
        }
        .to_owned()
    }
}
