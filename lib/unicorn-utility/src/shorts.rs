use serenity::client::Context;
use serenity::model::channel::Message;
use serenity::model::guild::Role;

pub struct UnicornShorts;

impl UnicornShorts {
    pub async fn get_roles(ctx: &Context, msg: &Message) -> Vec<Role> {
        let mut roles = Vec::<Role>::new();
        if let Some(guild) = msg.guild(&ctx.cache).await {
            for (_, role) in guild.roles.clone() {
                if let Ok(res) = msg.author.has_role(&ctx, guild.id, &role).await {
                    if res {
                        roles.push(role);
                    }
                }
            }
        };
        roles
    }
}
