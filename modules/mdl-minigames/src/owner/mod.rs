use unicorn_callable::UnicornCommand;

use crate::owner::generateresource::GenerateResourceCommand;

mod generateresource;

pub struct OwnerCommands;

impl OwnerCommands {
    pub fn commands() -> Vec<Box<dyn UnicornCommand>> {
        vec![GenerateResourceCommand::boxed()]
    }
}
