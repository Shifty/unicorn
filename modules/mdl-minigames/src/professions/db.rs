use std::any::Any;

use log::error;
use serde::{de::DeserializeOwned, Serialize};

use unicorn_database::DatabaseHandler;

#[serenity::async_trait]
pub trait DBCollection
    where
        Self: Any + Send + Sync + Sized + DeserializeOwned + Serialize + Clone,
{
    fn get_id(&self) -> &str;
    fn get_name(&self) -> &str;

    fn collection_name() -> &'static str;

    fn cache_key() -> String {
        format!("{}:all", Self::collection_name())
    }

    fn item_cache_key(id: &str) -> String {
        format!("{}:{}", Self::collection_name(), id)
    }

    async fn get_from_db_by_id(db: &DatabaseHandler, id: &str) -> Option<Self>;
    async fn get_all_from_db(db: &DatabaseHandler) -> Vec<Self>;

    async fn get_from_cache_by_id(db: &DatabaseHandler, id: &str) -> Option<Self> {
        let body = db.cache.get(Self::item_cache_key(id)).await?;
        match serde_json::from_str::<Self>(&body) {
            Ok(item) => Some(item),
            Err(why) => {
                error!("Failed desererializing item from cache: {}", why);
                None
            }
        }
    }

    async fn get_all_from_cache(db: &DatabaseHandler) -> Option<Vec<Self>> {
        let body = db.cache.get(Self::cache_key()).await?;
        match serde_json::from_str::<Vec<Self>>(&body) {
            Ok(items) => Some(items),
            Err(why) => {
                error!("Failed desererializing items cache: {}", why);
                None
            }
        }
    }

    async fn update_cache(db: &DatabaseHandler, items: Vec<Self>) {
        if !items.is_empty() {
            if let Ok(body) = serde_json::to_string(&items) {
                db.cache.set(Self::cache_key(), body).await;
            }
        }
    }

    async fn cache_item(db: &DatabaseHandler, item: Self) {
        if let Ok(body) = serde_json::to_string(&item) {
            db.cache
                .set(Self::item_cache_key(item.get_id()), body)
                .await;
        }
    }

    async fn get_all(db: &DatabaseHandler) -> Vec<Self> {
        if let Some(items) = Self::get_all_from_cache(db).await {
            items
        } else {
            let items = Self::get_all_from_db(db).await;
            Self::update_cache(db, items.clone()).await;
            items
        }
    }

    async fn get_by_id(db: &DatabaseHandler, id: &str) -> Option<Self> {
        if let Some(item) = Self::get_from_cache_by_id(db, id).await {
            Some(item)
        } else {
            let item = Self::get_from_db_by_id(db, id).await;
            if let Some(found_item) = item {
                Self::cache_item(db, found_item.clone()).await;
                Some(found_item)
            } else {
                None
            }
        }
    }

    async fn get_by_name(db: &DatabaseHandler, name: &str) -> Option<Self> {
        let mut result = None;
        for item in Self::get_all(db).await {
            if item.get_name().to_lowercase() == name.to_lowercase() {
                result = Some(item);
                break;
            }
        }
        result
    }
}
