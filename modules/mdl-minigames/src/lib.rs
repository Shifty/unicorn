use log::info;

use unicorn_callable::{UnicornCommand, UnicornEvent, UnicornReaction};
use unicorn_callable::module::UnicornModule;
use unicorn_config::builder::ConfigurationBuilder;
use unicorn_database::DatabaseHandler;

use crate::{
    ai::AICommands,
    database_init::DatabaseInit,
    gambling::{
        gamblingstats::GamblingStatsCommand,
        roulette::RouletteCommand,
        slots::SlotsCommand,
        slotstats::SlotStatsCommand,
    },
    owner::OwnerCommands,
    professions::ProfessionCommands,
    random::RandomCommands,
};

mod ai;
mod database_init;
mod gambling;
mod named;
mod owner;
mod professions;
mod random;

#[derive(Default)]
pub struct MinigamesModule;

impl MinigamesModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornModule for MinigamesModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            vec![
                SlotsCommand::boxed(),
                RouletteCommand::boxed(),
                GamblingStatsCommand::boxed(),
                SlotStatsCommand::boxed(),
            ],
            ProfessionCommands::commands(),
            OwnerCommands::commands(),
            RandomCommands::commands(),
            AICommands::commands(),
        ]
            .into_iter()
            .flatten()
            .collect()
    }

    fn reactions(&self) -> Vec<Box<dyn UnicornReaction>> {
        vec![ProfessionCommands::reactions()].into_iter().flatten().collect()
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Minigames"
    }

    async fn on_load(&self, db: &DatabaseHandler, _cfg: &mut ConfigurationBuilder) {
        info!("Checking profession items.");
        DatabaseInit::check_initialise(db).await;
    }
}
