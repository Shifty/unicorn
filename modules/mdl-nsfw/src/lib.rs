use unicorn_callable::module::UnicornModule;
use unicorn_callable::{UnicornCommand, UnicornEvent};

use crate::{
    boobs::BoobsCommand, butts::ButtsCommand, danbooru::DanbooruCommand, e621::E621Command,
    gelbooru::GelbooruCommand, konachan::KonachanCommand, rule34::Rule34Command,
    xbooru::XbooruCommand, yandere::YandereCommand,
};

mod common;

mod boobs;
mod butts;
mod danbooru;
mod e621;
mod gelbooru;
mod konachan;
mod rule34;
mod xbooru;
mod yandere;

#[derive(Default)]
pub struct NSFWModule;

impl NSFWModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornModule for NSFWModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            BoobsCommand::boxed(),
            ButtsCommand::boxed(),
            DanbooruCommand::boxed(),
            E621Command::boxed(),
            KonachanCommand::boxed(),
            YandereCommand::boxed(),
            GelbooruCommand::boxed(),
            Rule34Command::boxed(),
            XbooruCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "NSFW"
    }
}
