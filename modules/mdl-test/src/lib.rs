use unicorn_callable::module::UnicornModule;
use unicorn_callable::{UnicornCommand, UnicornEvent};
use unicorn_config::builder::ConfigurationBuilder;
use unicorn_database::DatabaseHandler;

use crate::{
    avatar::AvatarCommand, test::TestCommand, time::TimeCommand, timezone::TimezoneConfig,
};

mod avatar;
mod test;
mod time;
mod timezone;

#[derive(Default)]
pub struct TestModule;

impl TestModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornModule for TestModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            TestCommand::boxed(),
            TimeCommand::boxed(),
            AvatarCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Test"
    }

    async fn on_load(&self, _db: &DatabaseHandler, cfg: &mut ConfigurationBuilder) {
        cfg.add::<TimezoneConfig>();
    }
}
