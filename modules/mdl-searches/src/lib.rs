use unicorn_callable::module::UnicornModule;
use unicorn_callable::{UnicornCommand, UnicornEvent};

use crate::{
    crates::CratesCommand, deezer::DeezerCommand, imdb::IMDBCommand, kitsu::anime::AnimeCommand,
    kitsu::manga::MangaCommand,
};

mod crates;
mod deezer;
mod imdb;
mod kitsu;

#[derive(Default)]
pub struct SearchModule;

impl SearchModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornModule for SearchModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            DeezerCommand::boxed(),
            IMDBCommand::boxed(),
            AnimeCommand::boxed(),
            MangaCommand::boxed(),
            CratesCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Searches"
    }
}
