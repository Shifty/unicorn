use unicorn_callable::module::UnicornModule;
use unicorn_callable::{UnicornCommand, UnicornEvent};

use crate::{
    allguilds::GuildDumpCommand, eject::EjectCommand, geterror::GetErrorCommand, send::SendCommand,
    setavatar::SetAvatarCommand, shutdown::ShutDownCommand, sysexec::SysExecCommand,
};

mod allguilds;
mod eject;
mod geterror;
mod send;
mod setavatar;
mod shutdown;
mod sysexec;

#[derive(Default)]
pub struct OwnerModule;

impl OwnerModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornModule for OwnerModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            GuildDumpCommand::boxed(),
            SendCommand::boxed(),
            SysExecCommand::boxed(),
            EjectCommand::boxed(),
            GetErrorCommand::boxed(),
            SetAvatarCommand::boxed(),
            ShutDownCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Owner"
    }
}
