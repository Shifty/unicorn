use log::debug;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct SetAvatarCommand;

impl SetAvatarCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for SetAvatarCommand {
    fn command_name(&self) -> &str {
        "setavatar"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn owner(&self) -> bool {
        true
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["setav"]
    }

    fn description(&self) -> &str {
        "Sets the bot's avatar."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("url", false)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let url = if !pld.msg.attachments.is_empty() {
            if !pld.msg.attachments.is_empty() {
                Some(pld.msg.attachments[0].clone().url)
            } else {
                None
            }
        } else if pld.args.satisfied() {
            Some(pld.args.get("url").to_string())
        } else {
            None
        };
        let mut response = if let Some(url) = url {
            let pieces: Vec<&str> = url.split('.').collect();
            let ext = match pieces.last() {
                Some(ext) => match ext.to_owned() {
                    "jpg" => Some("jpg"),
                    "png" => Some("png"),
                    _ => None,
                },
                None => None,
            };
            if let Some(ext) = ext {
                match reqwest::get(&url).await {
                    Ok(resp) => {
                        if let Ok(bytes) = resp.bytes().await {
                            let b64 = base64::encode(&bytes.to_vec());
                            let avatar = format!("data:image/{};base64,{}", ext, b64);
                            let resp = pld
                                .ctx
                                .cache
                                .current_user()
                                .await
                                .edit(&&pld.ctx.http, |p| p.avatar(Some(&avatar)))
                                .await;
                            match resp {
                                Ok(_) => UnicornEmbed::ok("Avatar successfully changed."),
                                Err(why) => {
                                    debug!("Failed setting avatar: {}", why);
                                    UnicornEmbed::error("Failed to change my avatar.")
                                }
                            }
                        } else {
                            UnicornEmbed::error("Failed to read the image data.")
                        }
                    }
                    Err(why) => {
                        debug!("Failed getting URL: {}", why);
                        UnicornEmbed::error("Invalid URL.")
                    }
                }
            } else {
                UnicornEmbed::error("Must be a direct image URL.")
            }
        } else {
            UnicornEmbed::error("Missing image URL or attachment.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, |_| &mut response)
            .await?;
        Ok(())
    }
}
