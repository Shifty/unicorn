use unicorn_callable::module::UnicornModule;
use unicorn_callable::{UnicornCommand, UnicornEvent};

use crate::{
    commands::CommandsCommand, donate::DonateCommand, help::HelpCommand, invite::InviteCommand,
    repository::RepositoryCommand,
};

mod commands;
mod donate;
mod help;
mod invite;
mod repository;

#[derive(Default)]
pub struct HelpModule;

impl HelpModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornModule for HelpModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            CommandsCommand::boxed(),
            HelpCommand::boxed(),
            DonateCommand::boxed(),
            InviteCommand::boxed(),
            RepositoryCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Help"
    }
}
