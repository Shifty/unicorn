use unicorn_callable::module::UnicornModule;
use unicorn_callable::{UnicornCommand, UnicornEvent};

use crate::{donor::DonorCommand, team::TeamCommand};

mod common;
mod donor;
mod team;

#[derive(Default)]
pub struct FoldingAtHomeModule;

impl FoldingAtHomeModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornModule for FoldingAtHomeModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![DonorCommand::boxed(), TeamCommand::boxed()]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Folding at Home"
    }
}
