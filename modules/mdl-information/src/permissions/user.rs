use serenity::builder::CreateMessage;
use serenity::model::Permissions;
use serenity::model::guild::Member;
use serenity::http::CacheHttp;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::image::ImageProcessing;
use unicorn_utility::user::Avatar;

use crate::details::user::UserInfoCommand;

#[derive(Default)]
pub struct UserPermsCommand;

impl UserPermsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    async fn member_permissions(pld: &mut CommandPayload, member: Member) -> Option<Permissions> {
        if let Some(gld) = pld.msg.guild(&pld.ctx.cache).await {
            if let Ok(perms) = gld
                .member_permissions(&pld.ctx.http(), member.user.id)
                .await
            {
                Some(perms)
            } else {
                None
            }
        } else {
            None
        }
    }
}

#[serenity::async_trait]
impl UnicornCommand for UserPermsCommand {
    fn command_name(&self) -> &str {
        "userpermissions"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["userperms", "uperms"]
    }

    fn description(&self) -> &str {
        "Shows the mentioned user's permissions. If no user is mentioned, it will show permissions for the author."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("target", true).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let target = if pld.msg.mentions.is_empty() {
            if pld.args.has_argument("target") {
                UserInfoCommand::search(pld, pld.args.get("target")).await
            } else {
                UserInfoCommand::user_to_member(pld, pld.msg.author.clone()).await
            }
        } else {
            UserInfoCommand::user_to_member(pld, pld.msg.mentions[0].clone()).await
        };

        let mut response = if let Some(target) = target {
            if let Some(perms) = Self::member_permissions(pld, target.clone()).await {
                let mut msg = CreateMessage::default();
                let user = target.user.clone();
                let color = ImageProcessing::from_url(Avatar::url(user.clone())).await;
                let perm_names = perms.get_permission_names();
                let mut perms_block = "```\n".to_owned();
                for perm in perm_names {
                    perms_block.push_str(&format!("- {}\n", perm));
                }
                perms_block.push_str("```");
                msg.embed(|e| {
                    e.author(|a| {
                        a.name(format!("{}'s Permissions", user.name.clone()));
                        a.icon_url(Avatar::url(user.clone()));
                        a
                    });
                    e.color(color);
                    e.description(perms_block);
                    e
                });
                msg
            } else {
                UnicornEmbed::error("Failed to retrieve member permissions.")
            }
        } else {
            UnicornEmbed::not_found("User not found.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, |_| &mut response)
            .await?;
        Ok(())
    }
}
