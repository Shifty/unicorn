use serenity::builder::CreateMessage;
use serenity::model::Permissions;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::image::ImageProcessing;

#[derive(Default)]
pub struct FindCommand;

impl FindCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn perm_from_name(pld: &CommandPayload) -> Option<Permissions> {
        let mut perm = None;
        for i in 0..39 {
            if let Some(p) = Permissions::from_bits(1 << i) {
                let pnames = p.get_permission_names();
                if !pnames.is_empty() && pnames[0].to_lowercase() == pld.args.get("lookup").to_lowercase() {
                    perm = Some(p);
                    break
                }
            }
        }
        perm
    }
}

#[serenity::async_trait]
impl UnicornCommand for FindCommand {
    fn command_name(&self) -> &str {
        "roleswithpermission"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["rolewithperm", "rwperm", "rwp"]
    }

    fn description(&self) -> &str {
        "Shows all roles that have the specified permission."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("lookup", true).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if pld.args.has_argument("lookup") {
            if let Some(perm) = Self::perm_from_name(pld) {
                if let Some(guild) = pld.msg.guild(&pld.ctx.cache).await {
                    let mut msg = CreateMessage::default();
                    let (icon, color) = if let Some(icon) = guild.icon_url() {
                        (icon.clone(), ImageProcessing::from_url(icon.clone()).await)
                    } else {
                        ("https://i.imgur.com/QnYSlld.png".to_owned(), 0x1b_6f_5f)
                    };
                    let mut role_names = Vec::<String>::new();
                    for role in guild.roles.values() {
                        if role.has_permission(perm) {
                            let name = match role.managed {
                                true => format!("{}*", role.name),
                                false => role.name.clone()
                            };
                            role_names.push(name)
                        }
                    }
                    if !role_names.is_empty() {
                        let names = role_names.join(", ").replace('@', "@\u{200B}"); // zero-width space
                        msg.embed(|e| {
                            e.color(color);
                            e.author(|a| {
                                a.icon_url(&icon);
                                a.name(&guild.name);
                                a
                            });
                            e.description(names.clone());
                            if names.contains('*') {
                                e.footer(|f| {
                                    f.text("Roles with an asterisk are managed by an integration (most likely a bot)");
                                    f
                                });
                            }
                            e
                        });
                        msg
                    } else {
                         UnicornEmbed::not_found("No roles have that permission.")
                    }
                } else {
                    UnicornEmbed::error("Failed to get guild information.")
                }
            } else {
                let mut msg = CreateMessage::default();
                let perm_names = Permissions::all().get_permission_names();
                let desc = format!("The available permissions are: {}", perm_names.join(", "));
                msg.embed(|e| {
                    e.color(0x69_69_69);
                    e.title("🔍 No roles have that permission.");
                    e.description(desc);
                    e
                });
                msg
            }
        } else {
            UnicornEmbed::error("No lookup given.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, |_| &mut response)
            .await?;
        Ok(())
    }
}
