use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;

pub struct BotStatsCommand {
    stamp: i64,
}

impl Default for BotStatsCommand {
    fn default() -> Self {
        Self {
            stamp: chrono::Utc::now().timestamp(),
        }
    }
}

impl BotStatsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornCommand for BotStatsCommand {
    fn command_name(&self) -> &str {
        "statistics"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["stats"]
    }

    fn description(&self) -> &str {
        "Shows Sigma's current statistics. \
        Population, message and command counts, and rates since startup."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let cache = pld.ctx.cache.clone();
        let guild_count = pld.ctx.cache.guild_count().await;
        let mut role_count = 0u64;
        let mut user_count = 0u64;
        let channel_count = cache.guild_channel_count().await;
        for gid in &cache.guilds().await {
            if let Some(guild) = cache.guild(gid).await {
                role_count += guild.roles.len() as u64;
                user_count += guild.member_count;
            }
        }
        let _ = pld
            .msg
            .channel_id
            .send_message(&pld.ctx.http, |m| {
                m.embed(|e| {
                    e.color(0xf9_f9_f9);
                    e.title("📊 Instance Statistics");
                    e.field(
                        "Population",
                        format!(
                            "Guilds: **{}**\nChannels: **{}**\nRoles: **{}**\nUsers: **{}**",
                            guild_count, channel_count, role_count, user_count
                        ),
                        true,
                    );
                    e.field(
                        "Usage",
                        format!(
                            "Commands: **{}**\nCommand Rate: **{}**/s\nMessages: **{}**/s\nMessage Rate: **{}**",
                            "Unknown", "Unknown", "Unknown", "Unknown"
                        ),
                        true,
                    );
                    e.footer(|f| {
                        f.text(format!(
                            "Tracking since {} UTC.",
                            chrono::DateTime::<chrono::Utc>::from_utc(
                                chrono::NaiveDateTime::from_timestamp(self.stamp, 0),
                                chrono::Utc
                            )
                            .format("%e. %B %Y %T")
                        ));
                        f
                    });
                    e
                });
                m
            })
            .await;
        Ok(())
    }
}
