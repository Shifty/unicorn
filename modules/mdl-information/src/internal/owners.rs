use serenity::builder::CreateMessage;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;

#[derive(Default)]
pub struct BotOwnersCommand;

impl BotOwnersCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for BotOwnersCommand {
    fn command_name(&self) -> &str {
        "owners"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn description(&self) -> &str {
        "Shows a list of Sigma's owners. \
        Users in this list have access to the administration module."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut lines = Vec::new();
        for oid in &pld.cfg.discord.owners {
            if let Some(owner) = pld.ctx.cache.user(*oid).await {
                lines.push(format!("{}#{:0>4}", owner.name, owner.discriminator))
            } else {
                lines.push(oid.to_string());
            }
        }
        let mut response = CreateMessage::default();
        response.embed(|e| {
            e.color(0x3b_88_c3);
            e.title("🛡️ Bot Owners");
            e.description(lines.join(", "));
            e
        });
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, |_| &mut response)
            .await?;
        Ok(())
    }
}
