use unicorn_callable::module::UnicornModule;
use unicorn_callable::{UnicornCommand, UnicornEvent};

use crate::{
    filters::commands::{
        asciionlynames::ASCIIOnlyNamesCommand, asciitempname::ASCIITempNameCommand,
        blockwords::BlockWordsCommand,
    },
    preferences::{prefix::PrefixCommand, unflip::UnflipCommand},
};

mod filters;
mod preferences;

#[derive(Default)]
pub struct SettingsModule;

impl SettingsModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornModule for SettingsModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            ASCIIOnlyNamesCommand::boxed(),
            ASCIITempNameCommand::boxed(),
            BlockWordsCommand::boxed(),
            PrefixCommand::boxed(),
            UnflipCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Settings"
    }
}
