use serenity::http::CacheHttp;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::error::CallableError;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct ASCIIOnlyNamesCommand;

impl ASCIIOnlyNamesCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ASCIIOnlyNamesCommand {
    fn command_name(&self) -> &str {
        "asciionlynames"
    }

    fn category(&self) -> &str {
        "settings"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["forceascii"]
    }

    fn description(&self) -> &str {
        "Toggles if only ASCII characters are allowed in names.\
        The bot will check members' names when they join or update their profile\
        for any non ASCII characters and rename them if found.\
        To change the default temporary name, use the \"asciitempname\" command."
    }

    async fn discord_permissions(&self, pld: &mut CommandPayload) -> bool {
        if let Some(gld) = pld.msg.guild(&pld.ctx.cache).await {
            if let Ok(perms) = gld
                .member_permissions(pld.ctx.http(), &pld.msg.author.id)
                .await
            {
                perms.manage_guild()
            } else {
                false
            }
        } else {
            false
        }
    }

    async fn execute(&self, pld: &mut CommandPayload) -> Result<(), CallableError> {
        let mut response = if let Some(mut settings) = pld.settings.clone() {
            settings.ascii_only_names = !settings.ascii_only_names;
            settings.save(&pld.db).await;
            let state = if settings.ascii_only_names {
                "disabled"
            } else {
                "enabled"
            };
            UnicornEmbed::ok(format!("ASCII name enforcement has been {}.", state))
        } else {
            UnicornEmbed::error("Failed getting the guild's settings!")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, |_| &mut response)
            .await?;
        Ok(())
    }
}
