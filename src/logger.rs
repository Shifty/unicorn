use std::process::exit;

use fern::Dispatch;
use log::{error, info};

use unicorn_config::preferences::PreferencesConfig;

pub struct UnicornLogger;

impl UnicornLogger {
    pub fn init(cfg: &PreferencesConfig) {
        Self::check_dir();
        match Self::dispatch(cfg) {
            Ok(_) => {}
            Err(_) => {
                error!("Logger creation failed!");
                exit(22);
            }
        }
    }

    fn check_dir() {
        if !std::path::Path::new("logs").exists() {
            let _ = std::fs::create_dir("logs");
        }
    }

    fn dispatch(cfg: &PreferencesConfig) -> Result<(), anyhow::Error> {
        let level = if cfg.debug {
            log::LevelFilter::Debug
        } else {
            log::LevelFilter::Info
        };
        Dispatch::new()
            .format(|out, message, record| {
                out.finish(format_args!(
                    "{}[{}][{}] {}",
                    chrono::Local::now().format("[%Y-%m-%d %H:%M:%S]"),
                    record.target(),
                    record.level(),
                    message
                ))
            })
            .level(log::LevelFilter::Error)
            .level_for("unicorn", level)
            .level_for("unicorn_cache", level)
            .level_for("unicorn_callable", level)
            .level_for("unicorn_client", level)
            .level_for("unicorn_config", level)
            .level_for("unicorn_cooldown", level)
            .level_for("unicorn_database", level)
            .level_for("unicorn_embed", level)
            .level_for("unicorn_information", level)
            .level_for("unicorn_modman", level)
            .level_for("unicorn_resource", level)
            .level_for("unicorn_timer", level)
            .level_for("unicorn_utility", level)
            .chain(std::io::stdout())
            .chain(fern::log_file(format!(
                "logs/unc_{}.log",
                chrono::Local::now().format("%Y-%m-%d")
            ))?)
            .apply()?;
        info!("Unicorn logger created!");
        Ok(())
    }
}
